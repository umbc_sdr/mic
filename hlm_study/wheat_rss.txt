Y = cbind(wheat_data$V3, wheat_data$V4)

[1] "Metric 1 (RSS)"
[1] "OLS: 103514.627714"
[1] "ENV: 103464.541700"
[1] "HEM: 103086.937973"
[1] "Metric 2 (Residual covariance trace)"
[1] "OLS: 2095.758111"
[1] "ENV: 2108.024073"
[1] "HEM: 2099.947369"

Y = Y - cbind(rep(cmeans[1], 50), rep(cmeans[2], 50), rep(cmeans[3], 50), rep(cmeans[4], 50), rep(cmeans[5], 50), rep(cmeans[6], 50))

u = 1
[1] "Metric 1 (RSS)"
[1] "OLS: 338745.122735"
[1] "ENV: 338606.730718"
[1] "HEM: 337923.758943"
[1] "Metric 2 (Residual covariance trace)"
[1] "OLS: 6872.652800"
[1] "ENV: 6906.544722"
[1] "HEM: 6890.920833"

u = 3
[1] "Metric 1 (RSS)"
[1] "OLS: 338745.122735"
[1] "ENV: 338670.967249"
[1] "HEM: 337865.895739"
[1] "Metric 2 (Residual covariance trace)"
[1] "OLS: 6872.652800"
[1] "ENV: 6890.813327"
[1] "HEM: 6889.552375"

u = 5
[1] "Metric 1 (RSS)"
[1] "OLS: 338745.122735"
[1] "ENV: 338714.092851"
[1] "HEM: 337712.897902"
[1] "Metric 2 (Residual covariance trace)"
[1] "OLS: 6872.652800"
[1] "ENV: 6880.251955"
[1] "HEM: 6882.640766"

