full rank multivariate regression isn't really multivariate (regress predictors separately on each variable of the response)

https://cran.r-project.org/web/packages/rrr/vignettes/rrr.html

However, reduced rank regression is multivariate and requires the R rrr library.
