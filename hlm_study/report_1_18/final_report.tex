\title{Independent Study Fall 2017 Final Report}
\author{
        Sean Martin
}
\date{\today}

\documentclass[12pt]{article}

\usepackage{stackrel}
\usepackage{amsmath}
\usepackage{amssymb}  % assumes amsmath package installed
\usepackage{appendix}
\usepackage[pdftex]{graphicx} % for pdf, bitmapped graphics files
\usepackage{tabularx}
\usepackage{epsfig} % for postscript graphics files
%\usepackage{subfig}
\usepackage{stackrel}
\usepackage{subcaption}

\usepackage{lmodern}  % for bold teletype font
\usepackage{amsmath}  % for \hookrightarrow
\usepackage{xcolor}   % for \textcolor
\usepackage{listings}
\lstset{
  basicstyle=\ttfamily,
  columns=fullflexible,
  frame=single,
  breaklines=true,
  postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space},
}

\begin{document}
\maketitle

\begin{abstract}
This report details work performed towards the following two goals: development of a hierarchical envelope model for handling covariates falling into different clusters or groups, and development of an alternative information criterion for determining the dimension of a linear sufficient dimension reduction. This report presents an expectation maximization algorithm to address the first goal and shows some experimental results.  The report also shows how to modify the presented algorithm to use Kullback Liebler divergence instead of likelihood.  If successful this approach will address the second goal and provide an approach that is more computationally efficient and less prone to over/under estimation of the dimension.
\end{abstract}

\section{Introduction}

The work in this report focuses on the standard multivariate linear regression model:
\begin{align}
\label{eq1}
        Y = \alpha + \beta X + \epsilon
\end{align}
Where $Y \in R^r$ is the response vector, $X \in R^p$ is a non-stochastic vector of predictors, and $\epsilon \in R^r$ is normally distributed with mean 0 and unknown covariance $\Sigma$. More specifically this report focuses on the linear envelope model:
\begin{align}
\label{eq2}
        &Y = \alpha + \Gamma \eta X + \epsilon\\
        &\Sigma = \Sigma_1 + \Sigma_2 = \Gamma \Omega \Gamma^T + \Gamma_0 \Omega_0 \Gamma_0^T
\end{align}

Where $\Gamma \in R^{r\times u}$ is semi-orthogonal and $\eta \in R^{u \times p}$ is such that $\beta = \Gamma \eta$. Since more specialized models such as principle fitted components (PFC) can be derived from an envelope model, this report focuses on the linear envelope model.

Early work this semester examined a new information criterion (MLIC or Matrix Langevin information criterion) for calculating the dimension of $\Gamma$ based loosely on the approach taken in \cite{qiu2015new}. This work is detailed further in Appendix \ref{sec:mlic}.  However, it was determined that this approach gives more than a single min value during optimization and doesn't directly conform to a derivation from KL-divergence as AIC or BIC does. The derivation in Appendix \ref{sec:mlic} may be useful as a corrective term to either AIC or BIC, but additional analysis is needed. Regardless, a major drawback to this approach is that it requires potentially many measurements of the matrix distribution for $\Gamma$. In the context of linear regression, data for $\Gamma$ is not directly observable which leads to alternative approaches as discussed in section \ref{hem}.

As an alternative to information criterion for determining the dimension of $\Gamma$, the approach advocated in \cite{latecki2006new} estimates the dimension using an expectation maximization algorithm (EM) modified to use KL-divergence instead of likelihood during optimization. To explore this approach with envelopes, a new hierarchical model is derived in section \ref{hem} using an EM algorithm detailed in section \ref{sec_EM} similar to the approached used in other hierarchical models detailed in \cite{raudenbush2002hierarchical}. Modifications to use KL-divergence are discussed in section \ref{sec_KL} and experiments with the EM algorithm are discussed in sections \ref{diff_mean} and \ref{experiments}. Finally, conclusions and future work are outlined in sections \ref{conclusion} and \ref{future_work} respectively.

\section{Hierarchical Envelope Model}\label{hem}

This report considers the case where $\Gamma$ is distributed from a Matrix Langevin distribution over the Grassmann manifold as discussed in Appendix \ref{sec:mat_dist}. This leads to a hierarchical envelope model (HEM):
\begin{align*}
%\label{eq2}
        &\Gamma = M + \mu\\
        &Y = \alpha + \Gamma \eta X + \epsilon\\
        &\Sigma = \Sigma_1 + \Sigma_2 = \Gamma \Omega \Gamma^T + \Gamma_0 \Omega_0 \Gamma_0^T
\end{align*}
Where $\mu$ is matrix normal distributed as a multivariate normal of dimension $ru \times 1$ with: mean $vec\left(0\right)$, unknown variance $I_r \otimes \Sigma_u$, and the constraint that $\Gamma$ is orthonormal. A more detailed discussion of this is given in the appendix. Note this is similar to the lower dimension hierarchical linear models (HLMs) discussed by \cite{raudenbush2002hierarchical}. 

\section{The Expectation Maximization Algorithm}\label{conclusions}
\label{sec_EM}

The EM algorithm presented in \cite{raudenbush2002hierarchical} is modified here to operate on a hierarchical envelope model. Following the approach by Raudenbush, a block bivariate normal distribution is constructed to find the expected value and covariance of $\Gamma$. The maximization over likelihood to find $\Gamma$ and $\eta$ is then performed using the ManifoldOptim library \cite{martin2016manifoldoptim}. A full derivation along with code is shown in the appendix. Note that since $\Gamma$ lies on a Grassmann manifold and has a matrix variate distribution with constraints, theorems from both \cite{chikuse2012statistics} and \cite{gupta1999matrix} are used to derive the EM algorithm.


\section{KL-divergence with Grassmann Manifolds}
\label{sec_KL}
Research was performed to determine that the likelihood calculation used in the EM algorithm of section \ref{sec_EM} can be replaced with a KL-divergence estimation using kernels and non-parametric statistics to represent the true distribution \cite{latecki2006new}. Note that this should provide significant flexibility compared to other methods, since information criteria such as AIC, BIC, or MLIC can only be used with a specific distribution of fixed dimension on the manifold.  Using a non-parametric representation of the true distribution eliminates the need for choosing a specific distribution or specific matrix dimension and can allow both to be selected while running the EM algorithm. While the approach used by Latecki focuses on Gaussian mixture models, the approach should be adaptable to the HEM discussed in this report. As observed in Chikuse's work, a similar KL-divergence estimate can be constructed for comparing distributions on Grassmann manifolds \cite{chikuse2002methods}.

\section{The Difference of Means Problem}\label{diff_mean}

This problem involves comparing the means $\mu_1$ and $\mu_2$ of two multivariate normal populations. The model can be constructed by taking $\alpha = \mu_1$, $\beta = \mu_2 - \mu_1$, and $X \in {0,1}$. In this case $p=1$ which simplifies the construction of envelopes as discussed in \cite{cook2010envelope}. Specifically $\Sigma$ can be defined as:
\begin{align*}
        &\Sigma = \Sigma_1 + \Sigma_2 = \sigma^2 \Gamma \Gamma^T + \Gamma_0 \left(\sigma_0^2 I_9\right) \Gamma_0^T
\end{align*}
\section{Experiments} \label{experiments}
The experiments in this section focus on the difference of means problem.  For these experiments $r=10$, $p=1$, $u=5$, $\alpha^T = \mu_1^T = \left(0,\dots,0\right)$, $\beta^T = \mu_2^T = \left(\sqrt{10},\dots ,\sqrt{10}\right)$. Note that in Cook's experiments $u=1$. The results in section \ref{sec_repro} reproduce Cook's experiments found in \cite{cook2010envelope}.  Additionally, these results are generated with the new HEM instead of a standard linear envelope. The results in section \ref{sec_groups} show new work demonstrating the ability of the approach to detect whether or not the mean Gammas from different groups are equal or different by treating Gamma as a random effect.  Note that the code used in this section is listed as Appendix B.

\subsection{Reproduction of Cook's Tests}
\label{sec_repro}
While Cook used several versions of the generalized standard deviation ratio in comparing the efficiency of envelopes compared to full model OLS approaches, this section compares the true $\beta$ against the values estimated with each model.  Similar experiments are performed, namely varying $\sigma_0$ from 1 to 12. The Frobenius and $L1$ norms are calculated with OLS, envelopes, and HEM and plotted in Figure \ref{fig:l1_norms} and \ref{fig:fr_norms} respectively below. Note that the same data is used with each model and that some improvement is visible with both envelopes and HEM.

%trim=1cm 2cm 3cm 4cm, clip=true
% trim={<left> <lower> <right> <upper>}
\begin{figure}[htb!]
  \centering
  \epsfig{figure=l1_norms.png,trim=1cm 2cm 1cm 1.5cm, clip=true, width=9cm}
  \caption{L1 Norms between the true $\beta$ and the estimated values with: OLS, envelope, and HEM models.}
  \label{fig:l1_norms}
\end{figure}

\begin{figure}[htb!]
  \centering
  \epsfig{figure=fr_norms.png,trim=1cm 2cm 1cm 1.5cm, clip=true, width=9cm}
  \caption{Frobenius Norms between the true $\beta$ and the estimated values with: OLS, envelope, and HEM models.}
  \label{fig:fr_norms}
\end{figure}

%Cook used several versions of the generalized standard deviation ratio in comparing the efficiency of envelope vs. full model OLS approaches.  This section considers this ratio:

%\begin{align}
%  T = \left(tr\left(\Delta_{em}^{-1/2} \Delta_{fm} \Delta_{em}^{-1/2} \right) \right)
%\end{align}

%Where $\Delta_{fm}$ represents the covariance matrix of $\hat{\beta}_{fm}$, the estimator of $\beta$ from the full model CITE, and $\Delta_{em}$ represents the covariance matrix of $\hat{\beta}_{em}$ the estimator of $\beta$ from the envelope model CITE. Additionally, this section considers the ratio:

%\begin{align}
%  T = \left(tr\left(\Delta_{hem}^{-1/2} \Delta_{fm} \Delta_{hem}^{-1/2} \right) \right)
%\end{align}

%Where $\Delta_{hem}$ represents the covariance matrix of $\hat{\beta}_{hem}$, the estimator of $\beta$ from the hierchical envelope model.  Following Cook's second method, the values $\Delta_{fm}$, $\Delta_{em}$, and $\Delta_{hem}$ are estimated with the sample covariance matrices from 200 replications of $\hat{\beta}_{fm}$, $\hat{\beta}_{em}$, and  $\hat{\beta}_{hem}$ respectively.
\clearpage

\subsection{Variance of Gamma with Different Groups}
\label{sec_groups}
In this section instead of using a single randomly generated true $\Gamma$, two randomly generated $\Gamma$ values are created and used to generate half the $Y$ values each.  This section focuses on the case when $\sigma_0$ is set to 12. In table \ref{tab:table1} note that the L1 norms generated in this case are significantly higher for all models compared to the case when only a single $\Gamma$ value is used as in the previous section. Also Note that the HEM model slightly outperforms OLS and ENV in each case.

\begin{table}[h!]
	\centering
	\caption{Comparison of L1 Norm between true and estimated $\beta$ using samples generated from a single $\Gamma$ and with two $\Gamma$s. In each case $\sigma_0 = 12$.}
	\label{tab:table1}
	\begin{tabular}{l|l|l}
		Model & Single $\Gamma$ & Two $\Gamma$s \\
		\hline
		\rule{0pt}{2ex}    
		OLS & 13.500363& 17.037495\\
		\rule{0pt}{2ex}    
		ENV & 7.309597& 15.782556\\
		\rule{0pt}{2ex}    
		HEM & \textbf{5.882427}& \textbf{11.896287} \\
	\end{tabular}
\end{table}

Additionally, note that in table \ref{tab:table2} the sum of the eigenvalues for $\Sigma_u$, provided by the matrix trace, is over four times higher than the trace for $\Sigma_u$ when only a single true $\Gamma$ is used to generate all samples. This suggests that the estimated $\Sigma_u$ can be treated as a random effect and tested for differences within a population.

\begin{table}[h!]
	\centering
	\caption{Comparison of $tr\left(\Sigma_u\right)$ between samples generated from a single $\Gamma$ and with two $\Gamma$s. In each case $\sigma_0 = 12$.}
	\label{tab:table2}
	\begin{tabular}{l|l|l}
		Model & Single $\Gamma$ & Two $\Gamma$s \\
		\hline
		\rule{0pt}{2ex}    
		HEM & \textbf{0.001011751}& \textbf{0.004428971} \\
	\end{tabular}
\end{table}

\section{Conclusion} \label{conclusion}
This paper presented a new EM algorithm for handling estimation of a hierarchical envelope model (HEM) when examining covariates that fall into different groups or clusters. Simulation experiments conducted on the multivariate difference of means problem demonstrated, unexpectedly, that the HEM can outperform both OLS and linear envelopes at estimating $\beta$. In addition, the experiments in section \ref{sec_groups} suggest a way to determine whether different covariate groups exist. While in this paper it is assumed that parameters for a single group are being estimated, note that is is also possible to estimate parameters for groups when data can be categorized as coming from particular groups \cite{raudenbush2002hierarchical}.
This report also demonstrates progress in producing methods of estimating the dimension of $\Gamma$ for use in sufficient dimension reduction and in envelopes. Two approaches were considered: information criteria, and modification of the EM algorithm to use KL-divergence and estimate the dimension during estimation of other parameters. These approaches still need additional work to demonstrate their utility.

\section{Future Work} \label{future_work}
While promising, the results in this paper should be subjected to further scrutiny. By conducting Monte Carlo tests and examining convergence properties of the EM algorithm a greater understanding of the algorithm's reliability can be determined. Additionally, the further development of the KL-divergence metric and comparison to AIC, AICc, BIC, and BIC with corrected term is of primary interest in future work. This will enable estimation of $\Gamma$ without over or under estimating the dimension $u$. Additionally, application of HEM and information criteria to real data needs to be conducted. Furthermore, this data could potentially take the form of time series or longitudinal data.

\bibliographystyle{abbrv}
\bibliography{final_report}

\appendix

\section{Matrix Distributions}
\label{sec:mat_dist}
\subsection{Matrix Bingham Distribution}
\begin{align}
\label{dens:bingham}
\frac{1}{_1F_1\left(\frac{1}{2}k;\frac{1}{2}m;B\right)}etr\left(X^{'}BX\right)
\end{align}
Where $X$ is a point on the Stiefel manifold. Note that $B=Q_{m\times p}\Lambda_{p\times p}Q_{p\times m}^{'}$is of rank $p < m$.
\subsection{Matrix Langevin Distribution}
A slight modification to the Matrix Bingham distribution (REF) yeilds the Matrix Langevin distribution:
\begin{align}
\label{dens:langevin}
\frac{1}{_1F_1\left(\frac{1}{2}k;\frac{1}{2}m;B\right)}etr\left(BP\right)
\end{align}
Where $P=XX^{'}$ is a point on the Grassmann manifold for $X$ distributed as a matrix Bingham with distribution (REF). As before, note that $B=Q_{m\times p}\Lambda_{p\times p}Q_{p\times m}^{'}$is
of rank $p < m$. Note that the density \ref{dens:langevin} is obtained from the matrix normal distribution $N_{mm}\left(B,I_m\right)$ of $P$ on the space of all $m\times m$ symmetric matrices $S_m$ where the constraints $P^2=P$ and rank $P=k$ imposed. Also note that $B=0$ implies a uniform distribution.

\section{Matrix Langevin Information Criterion}
\label{sec:mlic}
This Appendix explores the Matrix Langevin information criterion (MLIC) derived through asymptotic evaluation similar to the approach used by \cite{qiu2015new}. While this approach provides an information criterion, the criterion is specific to the matrix Langevin distribution.  Thus, MLIC cannot determine whether another distribution defined over the Grassmann manifold (i.e. uniform) is a better option.
%\subsubsection{Unknown dimension of $k$}
Note that this criterion uses a first order approximation to both the hypergeometric function and the expected value of the Langevin density.

Let $P_{1},\ldots,P_{n}$ be iid from the Matrix Langevin distribution
$L^{(p)}\left(m,k,B\right)$ on the Grassmann manifold $P_{k,m-k}$ where
$P_{i}=X_{i}X_{i}^{'}$ for $X_{i}$ from the Stiefel manifold of dimension
$m\times k$. Note that $B=Q_{m\times p}\Lambda_{p\times p}Q_{p\times m}^{'}$is
of rank $p < m$.

\textbf{Claim1}:
$E\left(P_i\right)\approx Q\left(I_p-\frac{1}{2}\Lambda^{\frac{1}{2}}\left(r\left(I_p + a\Lambda^{-1}\right)\right)\Lambda^{\frac{1}{2}}\right)Q^{'}$ for $r=m-k$ and $a = \frac{1}{2}\left(p-k+1\right)$.\\
From Chikuse pg 375 let 
\begin{align*}
&X_i = 2\Lambda^{\frac{1}{2}}\left(I_p-Q^{-1}P_iQ\right)\Lambda^{\frac{1}{2}}\\
\Rightarrow&P_i = Q\left(I_p-\frac{1}{2}\Lambda^{-\frac{1}{2}}X_i\Lambda^{-\frac{1}{2}}\right)Q^{'}
\end{align*}
By a first order approximation:
\begin{align*}
&E\left(X\right)=r\left(I_p+a\Lambda^{-1}\right)+o\left(\Lambda^{-2}\right)\approx r\left(I_p+a\Lambda^{-1}\right)\\
\Rightarrow&E\left(P_i\right)=E\left(Q\left(I_p-\frac{1}{2}\Lambda^{\frac{-1}{2}}X_i\Lambda^{\frac{-1}{2}}\right)Q^{'}\right)\approx Q\left(I_p-\frac{1}{2}\Lambda^{\frac{-1}{2}}\left(r\left(I_p+a\Lambda^{-1}\right)\right)\Lambda^{\frac{-1}{2}}\right)Q^{'}
\end{align*}

\textbf{Claim2}:
$tr\left(BE\left(P\right)\right)\approx tr\left(Q\Lambda Q^{'}-\frac{r}{2}I_m -\frac{a}{2}Q\Lambda^{-1}Q^{'}\right)$\\
By the definition of B and Claim1:
\begin{align*}
tr\left(BE\left(P\right)\right)&\approx tr\left(Q\left(I_p-\frac{1}{2}\Lambda^{\frac{1}{2}}\left(r\left(I_p + a\Lambda^{-1}\right)\right)\Lambda^{\frac{1}{2}}\right)Q^{'}\right)\\
&=tr\left(Q\Lambda Q^{'}-\frac{r}{2}Q\Lambda\Lambda^{-1/2}I_p\Lambda^{-1/2}Q^{'}-\frac{a}{2}Q\Lambda\Lambda^{-1/2}\Lambda^{-1}\Lambda^{-1/2}Q^{'}\right)\\
&=tr\left(Q\Lambda Q^{'}-\frac{r}{2}I_m-\frac{a}{2}Q\Lambda^{-1} Q^{'}\right)
\end{align*}
where the last line makes use of the fact that $Q$ is orthogonal and thus $Q^{'}Q=I$, and also the properties of trace (i.e. $tr\left(QQ^{'}\right)=tr\left(Q^{'}Q\right)$).

\textbf{Theorem1}:
Let $P_{1},\ldots,P_{n}$ be iid from the Matrix Langevin distribution
$L^{(p)}\left(m,k,B\right)$ on the Grassmann manifold $P_{k,m-k}$. Then the matrix Langevin information criterion (MLIC) defined as:
\begin{align}
MLIC=-\frac{1}{n}lgL_n+2lg\left(2m+2ktr\left(B\right)\right)-2tr\left(Q\Lambda Q^{'}-\frac{r}{2}I_m-\frac{a}{2}Q\Lambda^{-1} Q^{'}\right)
\end{align}
converges almost surely to a constant for any value of k, where $L_n$ is the sample likelihood.

\textbf{Proof}:
Assuming large values of $N$ and $m$, a first order approximation to the hypergeometric function is (Chikuse pg 318): 
\begin{align*}
_1F_1\left(a;N;A\right)\approx 1 + \frac{a}{N} tr\left(A\right)
\end{align*}
Thus:
\begin{align*}
lgLn & = lg\stackrel[i=1]{n}{\prod}\frac{1}{_1F_1\left(\frac{1}{2}k;\frac{1}{2}m;B\right)}etr\left(BP_i\right)\\
& = \stackrel[i=1]{n}{\sum}-lg\left(_1F_1\left(\frac{1}{2}k;\frac{1}{2}m;B\right)\right)+\stackrel[i=1]{n}{\sum}tr\left(BP_i\right)\\
& \approx \stackrel[i=1]{n}{\sum}-lg\left(\frac{2m}{2m}+\frac{2k}{2m}tr\left(B\right)\right)+\stackrel[i=1]{n}{\sum}tr\left(BP_i\right)\\
& = -n\left(lg\left(2m+2ktr\left(B\right)\right)-lg\left(2m\right)\right)+\stackrel[i=1]{n}{\sum}tr\left(BP_i\right)\\
\Rightarrow \frac{1}{n}lgLn & \approx -lg\left(2m+2ktr\left(B\right)\right)+lg\left(2m\right) + \frac{1}{n}\stackrel[i=1]{n}{\sum}tr\left(BP_i\right)\\
& = -lg\left(2m+2ktr\left(B\right)\right)+lg\left(2m\right) + tr\left(B\stackrel[i=1]{n}{\sum}\frac{P_i}{n}\right)\\
& \stackrel{a.s.}{\rightarrow} -lg\left(2m+2ktr\left(B\right)\right)+lg\left(2m\right) + tr\left(BE\left(P_i\right)\right)\\
& = -lg\left(2m+2ktr\left(B\right)\right)+lg\left(2m\right) + tr\left(Q\Lambda Q^{'}-\frac{r}{2}I_m -\frac{a}{2}Q\Lambda^{-1} Q^{'}\right)
\end{align*}
Where the third to the last equality is by properties of trace.  Since $E\left(P_i\right)< \infty$, the convergence in the second to the last line holds by the strong law of large numbers (SLLN).  The last equality holds by Claim 2.

At this point note that the likelihood $L\left(m,k,B|P_i\right)$ is a random variable.  Since $E\left(|L\left(m,k,B|P_i\right)|\right)<\infty$, by the SLLN:
\begin{align*}
\frac{1}{n}lgLn&= \frac{1}{n}lg\left(\stackrel[i=1]{n}{\prod}L\left(m,k,B|P_i\right)\right)\\
&=\frac{1}{n}lg\left(\stackrel[i=1]{n}{\sum}L\left(m,k,B|P_i\right)\right)\\
&\stackrel{a.s.}{\rightarrow}c
\end{align*}
for some constant $c$. Since $X_iX_i^{'}=P_i$ for $X_i$ on a Stiefel manifold, the dimension $k$ for $X_i$ cannot be directly estimated. Thus, to construct a metric that is constant w.r.t. $k$, information for different candidates of $k$ can be compared.  Also observe that:
\begin{align*}
MLIC&=\frac{1}{n}lgLn + lg\left(2m+2ktr\left(B\right)\right) - tr\left(Q\Lambda Q^{'}-\frac{r}{2}I_m -\frac{a}{2}Q\Lambda^{-1} Q^{'}\right)\\
& \stackrel{a.s.}{\rightarrow}  c^{'}
\end{align*}
for a constant $c^{'}$ that does not depend on $k$.

\section{HEM Source Code}

\lstinputlisting[language=R]{../mv_norm_mean.R}
%\lstinputlisting{../mv_norm_mean.R}

\end{document}
