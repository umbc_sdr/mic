// [[Rcpp::depends(RcppArmadillo)]]
#include <RcppArmadillo.h>
#include <iostream>

using namespace std;

// normalize columns of a matrix
void normalizeVec(arma::mat &S){

  for(size_t idx = 0; idx < S.n_cols; ++idx){
    S.col(idx) = S.col(idx) / arma::sum(S.col(idx));
  }

}

// Build a block diagonal matrix 
// x: list of matrices 
arma::mat blockDiag(arma::field<arma::mat> &x) {

  unsigned int n = x.n_rows ;
  int dimen = 0 ;
  arma::ivec dimvec(n);

  for(unsigned int i=0; i<n; i++) {
    dimvec[i] = x(i,0).n_rows ; 
    dimen += dimvec[i] ;
  }

  arma::mat X(dimen,dimen,arma::fill::zeros);
  int idx=0;

  for(unsigned int i=0; i<n; i++) {

    // skip matrices of size 0
    if(dimvec[i] == 0)
      continue;

    X.submat( idx, idx, idx + dimvec[i] - 1, idx + dimvec[i] - 1 ) = x(i,0) ;
    idx = idx + dimvec[i] ;
  }

  return(X);
}

arma::mat generateCandidate(size_t m, size_t k){
  // arma::mat candidate = arma::eye<arma::mat>(m,m);
  // return candidate;

  while(true){
  
    // start by sampling from N(0,1) to fill a m by k matrix
    arma::mat U = arma::randn(m,k);

    arma::mat U_tmp = arma::pow(U.t() * U, -.5);

    //cout << U_tmp << endl;

    if(arma::is_finite(U_tmp)){

      // Generate Stiefel 
      arma::mat X = U * U_tmp;
    
      // Generate Grassmann from Stiefel
      arma::mat P = X * X.t();
    
      return P;
    }
  }
}

arma::mat generateCandidate2(size_t m, size_t k){

  // generate random thetas
  arma::mat thetas = arma::randu(m * (m-1) / 2,1) * 2 * arma::datum::pi;

  // This will be the constructed orthogonal matrix
  arma::mat S = arma::eye<arma::mat>(m+2,m+2);//diag(m+2)

  // index into angles
  size_t idx = 0;

  // Note that S can be represented as the product of m(m-1)/2 orthogonal 
  // matrices R^v_m(theta), so construct S here.
  //  for (v in 1:(m-1)) {
  for(size_t v = 1; v <= m-1; ++v){

    //    S_vm = diag(m+2)
    arma::mat S_vm = arma::eye<arma::mat>(m+2,m+2);

    //for (j in (m-1):v) {
    for(size_t j = m-1; j >= v; --j){

      // randomly select one of the binned angles
      double theta = thetas(idx, 0);

      // generate R_jm 
      //Rcpp::List blk_mats(3);
      arma::field<arma::mat> blk_mats(3);
      blk_mats[0] = arma::eye<arma::mat>(j-1,j-1);
      
      double rot_mat_a[] = {cos(theta), -sin(theta), sin(theta), cos(theta)};
      arma::mat rot_mat(rot_mat_a, 2, 2);
      blk_mats[1] = rot_mat;

      blk_mats[2] = arma::eye<arma::mat>(m-(j-1),m-(j-1));

      //R_jm = bdiag(diag(j-1), matrix(c(cos(theta), -sin(theta), sin(theta), cos(theta)),nrow=2),diag(m-(j-1)))
      arma::mat R_jm = blockDiag(blk_mats);
      //cout << R_jm << endl;

      S_vm = S_vm * R_jm;

      idx = idx + 1;
    }
    
    S = S * S_vm;
  }

  //cout << "DONE" << endl;
  //cout << S << endl;

  // Generate Stiefel
  //  X = matrix(S[1:m,1:k],ncol=k)
  arma::mat X = S.submat( 0,0,m-1,k-1 );
  normalizeVec(X);

  //cout << X << endl;

  // Generate Grassmann from Stiefel
  arma::mat P = X * X.t();

  return P;

}


// [[Rcpp::export]]
Rcpp::List genGrassmanSamplesArma(size_t m, size_t k, size_t n, arma::mat B){

  Rcpp::List samples(n);

  size_t cnt = 0 ;


  // eigenvalue problem
  arma::vec eigval;
  arma::mat eigvec;
  arma::eig_sym(eigval, eigvec, B); 

  double ev_sum = arma::sum(eigval);

  while (cnt < n) {

    arma::mat U = arma::randu(1);
    double u = U(0,0);
    arma::mat candidate = generateCandidate2(m, k);
    arma::mat tmp = B * candidate;

    //    cout << u << " " << exp(arma::sum(tmp.diag()) - arma::sum(B.diag())) << endl;
    //cout << u << " " << arma::sum(tmp.diag()) << " " <<  arma::sum(B.diag()) << endl;

    //cout << "P: " << candidate << endl << "TMP: " << tmp << endl << "B: " << B << endl;

    if (ev_sum == 0 || u < exp(arma::sum(tmp.diag()) - ev_sum)) {
      samples[cnt] = candidate;
      cnt++;
      cout << cnt << endl;
    }else{
      //cout << "FAILED" << endl;
    }
  }

  return(samples);
}







// SRM clean up the following two functions

arma::mat generateCandidate3(size_t m, size_t k){

  // generate random thetas
  arma::mat thetas = arma::randu(m * (m-1) / 2,1) * 2 * arma::datum::pi;

  // This will be the constructed orthogonal matrix
  arma::mat S = arma::eye<arma::mat>(m+2,m+2);//diag(m+2)

  // index into angles
  size_t idx = 0;

  // Note that S can be represented as the product of m(m-1)/2 orthogonal 
  // matrices R^v_m(theta), so construct S here.
  //  for (v in 1:(m-1)) {
  for(size_t v = 1; v <= m-1; ++v){

    //    S_vm = diag(m+2)
    arma::mat S_vm = arma::eye<arma::mat>(m+2,m+2);

    //for (j in (m-1):v) {
    for(size_t j = m-1; j >= v; --j){

      // randomly select one of the binned angles
      double theta = thetas(idx, 0);

      // generate R_jm 
      //Rcpp::List blk_mats(3);
      arma::field<arma::mat> blk_mats(3);
      blk_mats[0] = arma::eye<arma::mat>(j-1,j-1);
      
      double rot_mat_a[] = {cos(theta), -sin(theta), sin(theta), cos(theta)};
      arma::mat rot_mat(rot_mat_a, 2, 2);
      blk_mats[1] = rot_mat;

      blk_mats[2] = arma::eye<arma::mat>(m-(j-1),m-(j-1));

      //R_jm = bdiag(diag(j-1), matrix(c(cos(theta), -sin(theta), sin(theta), cos(theta)),nrow=2),diag(m-(j-1)))
      arma::mat R_jm = blockDiag(blk_mats);
      //cout << R_jm << endl;

      S_vm = S_vm * R_jm;

      idx = idx + 1;
    }
    
    S = S * S_vm;
  }

  //cout << "DONE" << endl;
  //cout << S << endl;

  // Generate Stiefel
  //  X = matrix(S[1:m,1:k],ncol=k)
  arma::mat X = S.submat( 0,0,m-1,k-1 );
  normalizeVec(X);

  //cout << X << endl;

  // Generate Grassmann from Stiefel
  //  arma::mat P = X * X.t();

  return X;

}


// [[Rcpp::export]]
Rcpp::List genStiefelSamplesArma(size_t m, size_t k, size_t n, arma::mat B){

  Rcpp::List samples(n);

  size_t cnt = 0 ;


  // eigenvalue problem
  arma::vec eigval;
  arma::mat eigvec;
  arma::eig_sym(eigval, eigvec, B); 

  double ev_sum = arma::sum(eigval);

  while (cnt < n) {

    arma::mat U = arma::randu(1);
    double u = U(0,0);
    arma::mat candidate = generateCandidate3(m, k);
    arma::mat tmp = B * candidate * candidate.t();

    //    cout << u << " " << exp(arma::sum(tmp.diag()) - arma::sum(B.diag())) << endl;
    //cout << u << " " << arma::sum(tmp.diag()) << " " <<  arma::sum(B.diag()) << endl;

    //cout << "P: " << candidate << endl << "TMP: " << tmp << endl << "B: " << B << endl;

    if (ev_sum == 0 || u < exp(arma::sum(tmp.diag()) - ev_sum)) {
      samples[cnt] = candidate;
      cnt++;
      cout << cnt << endl;
    }else{
      //cout << "FAILED" << endl;
    }
  }

  return(samples);
}
