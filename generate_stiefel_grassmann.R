# Date Created: 3/2/17
# Description:
# This is a routine to create samples from both a Stiefel and a Grassmann 
# manifold based on Chikuse's proceedure described in:
#
# Cetingul, Hasan Ertan, and René Vidal. "Intrinsic mean shift for clustering 
# on Stiefel and Grassmann manifolds." Computer Vision and Pattern 
# Recognition, 2009. CVPR 2009. IEEE Conference on. IEEE, 2009.
#
# Chikuse, Yasuko. Statistics on special manifolds. Vol. 174. Springer 
# Science & Business Media, 2012.
genStiefelGrassmann <- function(type, m, k, thetas) {

library(Matrix)

if (nargs() < 4){
   # select bins in [0,2*pi) # SRM NOTE: check this
   thetas = runif(m * (m-1) / 2,0,2*pi)   
   print("Generated random thetas.")
}

# This will be the constructed orthogonal matrix
S = diag(m+2)

# index into angles
idx = 1

# Note that S can be represented as the product of m(m-1)/2 orthogonal 
# matrices R^v_m(theta), so construct S here.
    print("Before")
for (v in 1:(m-1)) {

    S_vm = diag(m+2)

    for (j in (m-1):v) {

    	# randomly select one of the binned angles
    	#idx = runif(1,1,m*(m-1)/2)
    	theta = thetas[idx]

	# generate R_jm 
    	R_jm = bdiag(diag(j-1), matrix(c(cos(theta), -sin(theta), sin(theta), cos(theta)),nrow=2),diag(m-(j-1)))
        #print(R_jm)
        
        #S = S %*% R_jm
	S_vm = S_vm %*% R_jm
    	#print(paste0("v: ", v, " j: ", j, " theta: ", theta))

	idx = idx + 1
    }
    #print(S_vm)
    S = S %*% S_vm
}
    print("After")
    
# Generate Stiefel
X = matrix(S[1:m,1:k],ncol=k)
#print(X %*% t(X))
#print(X)
    
if(type == "Stiefel"){

return(list(X,thetas))

}else if(type == "Grassmann"){

# Generate Grassmann from Stiefel
P = X %*% t(X)

return(list(P,thetas))

}else{

print("ERROR: unknown type.")

}

}
